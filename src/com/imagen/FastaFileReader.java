/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

class FastaFileReader {

    private int columnsLength;
    private String fastaHeader;
    private String fastaAbsolutePath;
    private String fastaName;
    private String fastaFolder;
    private String folderOutput;
    private boolean lastLineReached;
    private char lastSymbol;
    private StringBuilder nonAtcgMap;
    private int currentPosition;
    private int initialNonATCGPosition;
    private Map<Character, Integer> symbolsAmount;
    private StringBuilder atcgSequence;
    private int delta;

    FastaFileReader(CommandValidator commandValidator) {

        this.symbolsAmount = new HashMap<>();
        this.atcgSequence = new StringBuilder();
        this.nonAtcgMap = new StringBuilder();
        this.folderOutput = commandValidator.getOutputPath();
        this.lastLineReached = false;
        this.lastSymbol = '#';
        this.currentPosition = 0;
        this.initialNonATCGPosition = 0;
        this.delta = 0;


        try {

            File fastaFile = new File(commandValidator.getInputPath());

            this.setPaths(fastaFile);

            BufferedReader reader = new BufferedReader(new FileReader(this.fastaAbsolutePath));

            // reading first line (fasta header)
            this.fastaHeader = reader.readLine();

            // reading the second line (first atcg line)
            String firstATCGLine = reader.readLine();

            // set column length
            this.columnsLength = firstATCGLine.length();

            this.removeNonATCG(firstATCGLine);

            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                this.removeNonATCG(currentLine);
            }

            // treat the last symbol
            this.lastLineReached = true;
            this.removeNonATCG("");

            reader.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void removeNonATCG(String line) {
        // Resolve last character if it is different from ATCG
        if (this.lastLineReached) {
            if (this.lastSymbol != '#' && !this.isAtcgSymbol(this.lastSymbol)) {
                int length = this.currentPosition - this.initialNonATCGPosition;
                this.nonAtcgMap.append(String.valueOf(length));
                this.nonAtcgMap.append(System.lineSeparator());
            }
        } else {
            for (int index = 0; index < line.length(); index++) {
                char c = Character.toUpperCase(line.charAt(index));
                if (c != ' ') {

                    if (this.symbolsAmount.containsKey(c)) {
                        this.symbolsAmount.put(c, this.symbolsAmount.get(c) + 1);
                    } else {
                        this.symbolsAmount.put(c, 1);
                    }

                    if (isAtcgSymbol(c)) {
                        this.atcgSequence.append(Character.toString(c));
                        if (this.lastSymbol != '#' && !this.isAtcgSymbol(this.lastSymbol)) {
                            int length = this.currentPosition - this.initialNonATCGPosition;
                            this.nonAtcgMap.append(String.valueOf(length));
                            this.nonAtcgMap.append(System.lineSeparator());
                        }
                    } else {
                        if (c != this.lastSymbol) {
                            if (this.lastSymbol != '#' && !this.isAtcgSymbol(this.lastSymbol)) {
                                int length = this.currentPosition - this.initialNonATCGPosition;
                                this.nonAtcgMap.append(String.valueOf(length));
                                this.nonAtcgMap.append(System.lineSeparator());
                            }
                            this.delta = this.currentPosition - this.initialNonATCGPosition;
                            this.initialNonATCGPosition = this.currentPosition;
                            this.nonAtcgMap.append(String.valueOf(this.delta));
                            this.nonAtcgMap.append(Character.toString(c));
                        }
                    }
                    this.currentPosition += 1;
                    this.lastSymbol = c;
                }
            }
        }
    }

    private boolean isAtcgSymbol(char c) {
        // return c == 'a' || c == 'c' || c == 'g' || c == 't';
        return c == 'A' || c == 'C' || c == 'G' || c == 'T';
    }

    private void setPaths(File fastaFile) {
        this.fastaAbsolutePath = fastaFile.getAbsolutePath();
        this.fastaName = fastaFile.getName();
        this.fastaFolder = fastaFile.getParent();
    }

    int getColumnsLength() {
        return columnsLength;
    }

    String getFastaHeader() {
        return fastaHeader;
    }

    String getFastaAbsolutePath() {
        return fastaAbsolutePath;
    }

    String getFastaName() {
        return fastaName;
    }

    String getFastaFolder() {
        return fastaFolder;
    }

    String getFolderOutput() {
        return folderOutput;
    }

    StringBuilder getNonAtcgMap() {
        return nonAtcgMap;
    }

    Map<Character, Integer> getSymbolsAmount() {
        return symbolsAmount;
    }

    StringBuilder getAtcgSequence() {
        return atcgSequence;
    }

    public void setAtcgSequence(StringBuilder atcgSequence) {
        this.atcgSequence = atcgSequence;
    }
}

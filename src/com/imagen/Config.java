/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import java.util.Arrays;
import java.util.List;

public class Config {

    /**
     * The instance of Config that this Class is storing
     */
    private static Config instance = null;

    String[] extensionsAllowedInCompression = new String[]{"fasta", "fa"};
    String[] extensionsAllowedInDecompression = new String[]{"flif", "webp"};
    String extensionCodebook = ".txt";
    String extensionZippedCodebook = ".7z";
    String extensionsAllowedCodebook = extensionCodebook + extensionZippedCodebook;
    int maxWebpWidth = 16384;
    int maxWebpHeight = 16384;
    String fastaHeaderKey = "h";
    String fastaSequenceLengthKey = "l";
    String fastaColumnsLengthKey = "c";
    String transformersKey = "t";
    public String bwtKey = "b";
    public String mtfKey = "m";
    public String rleKey = "r";
    public String btcKey = "c";
    public List<String> transforms = Arrays.asList(
            bwtKey,
            mtfKey,
            rleKey,
            btcKey
    );

    private Config() {
        // Avoid create an instance outside of this class
    }


    public static Config getInstance() {
        if (Config.instance == null) {
            Config.instance = new Config();
        }

        return Config.instance;
    }
}

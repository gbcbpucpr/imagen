/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import com.imagen.JSON.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;


class CodeBookWriter {

    private CodeBook codeBook;
    private Config config = Config.getInstance();

    CodeBookWriter(CodeBook codeBook) {

        this.codeBook = codeBook;
    }

    public void write() {

        try {

            Map<String, Object> map = new HashMap<>();

            map.put(config.fastaHeaderKey, codeBook.getFastaHeader());
            map.put(config.fastaSequenceLengthKey, codeBook.getSequenceLength());
            map.put(config.fastaColumnsLengthKey, codeBook.getColumnsLength());
            map.put(config.transformersKey, codeBook.getTransforms());

            JSONObject json = new JSONObject(map);

            FileWriter fileWriter = new FileWriter(codeBook.getPathToSave());

            // write header
            fileWriter.write(String.valueOf(json) + "\n");

            // write non atcg map
            fileWriter.write(String.valueOf(codeBook.getNonAtcgMap()));

            fileWriter.flush();
            fileWriter.close();


            // System.exit(0);

            File codebookTxt = new File(codeBook.getPathToSave());

            if (!codebookTxt.isFile()) {
                throw new Exception("The created codebook is not a file.");
            }

            String zippedCodebook = codeBook.getPathToSave() + config.extensionZippedCodebook;

            CodeBookZip.encode(codebookTxt, zippedCodebook);

            codebookTxt.delete();

            if (codebookTxt.exists()) {
                System.out.println("Failed to delete the txt Codebook file on compression.");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import org.apache.commons.cli.ParseException;

import java.io.File;
import java.util.Arrays;
import java.util.List;

class InOutPathValidator {

    private Config config = Config.getInstance();

    /**
     * Method for validate the input path.
     *
     * @param inputPath
     * @param compress
     * @throws ParseException
     */
    void validateInputPath(String inputPath, boolean compress) throws ParseException {

        String[] allowedExtension = config.extensionsAllowedInCompression;
        String extension = inputPath.substring(inputPath.lastIndexOf(".") + 1);
        File tempDirectory = new File(inputPath);

        if (!compress) {
            allowedExtension = config.extensionsAllowedInDecompression;

            String codebookPath = tempDirectory.getParent() + "/" + tempDirectory.getName().replaceFirst("[.][^.]+$", "");

            String zippedCodebook = codebookPath + config.extensionsAllowedCodebook;

            File zip = new File(zippedCodebook);
            if (!zip.exists() && !zip.isFile()) {
                throw new ParseException("The helper file for action 'd' is missing.");
            }
        }

        List<String> listExtensions = Arrays.asList(allowedExtension);

        if (!listExtensions.contains(extension)) {
            throw new ParseException("The extension file must be equal to: " + listExtensions.toString() + ".");
        }

        if (!tempDirectory.exists()) {
            throw new ParseException("The " + inputPath + " path does not exist.");
        }

        if (!tempDirectory.isFile()) {
            throw new ParseException("The " + inputPath + " path is not a file.");
        }
    }

    /**
     * Method for validate the output path.
     *
     * @param outputPath
     * @throws ParseException
     */
    void validateOutputFolder(String outputPath) throws ParseException {

        File tempDirectory = new File(outputPath);

        if (!tempDirectory.exists()) {
            throw new ParseException("The " + outputPath + " path does not exist.");
        }
        if (!tempDirectory.isDirectory()) {
            throw new ParseException("The " + outputPath + " path is not a directory.");
        }
    }
}

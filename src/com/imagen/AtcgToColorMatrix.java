/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import com.imagen.tools.converters.SymbolColor;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

class AtcgToColorMatrix {

    private ArrayList<BufferedImage> bufferedImageList;

    AtcgToColorMatrix(StringBuilder sequence, boolean isWebp) {

        Config config = Config.getInstance();

        int maxLength = config.maxWebpWidth * config.maxWebpHeight;

        bufferedImageList = new ArrayList<>();

        if (sequence.length() > maxLength && isWebp) {

            int start = 0;
            int end = maxLength - 1;
            while (start <= end) {
                StringBuilder subSequence = new StringBuilder(sequence.substring(start, end));
                BufferedImage bufferedImage = matrixGenerator(subSequence);
                bufferedImageList.add(bufferedImage);
                start += maxLength;
                end += maxLength;
                if (end >= sequence.length()) {
                    end = sequence.length() - 1;
                }
            }

        } else {
            BufferedImage bufferedImage = matrixGenerator(sequence);
            bufferedImageList.add(bufferedImage);
        }
    }

    /**
     * Generate matrix of RGB colors from sequence
     *
     * @param sequence
     * @return
     */
    private BufferedImage matrixGenerator(StringBuilder sequence) {

        BufferedImage image = null;

        try {

            Double relativity = Math.sqrt(sequence.length());

            int lines = 0;
            int columns = 0;

            if (relativity % 2 == 0) {

                lines = relativity.intValue();
                columns = relativity.intValue();

            } else if ((relativity - Math.floor(relativity)) >= 0.5d) {

                lines = (int) Math.ceil(relativity);
                columns = (int) Math.ceil(relativity);

            } else if ((relativity - Math.floor(relativity)) < 0.5d) {

                lines = (int) Math.ceil(relativity);
                columns = (int) Math.floor(relativity);

            }

            image = new BufferedImage(columns, lines, BufferedImage.TYPE_INT_RGB);

            int currentLine = 0;
            int currentColumn = 0;

            for (int currentPosition = 0; currentPosition < sequence.length(); currentPosition++) {

                char letter = sequence.charAt(currentPosition);

                Color color = SymbolColor.symbolToColor(letter);

                image.setRGB(currentColumn, currentLine, color.getRGB());

                currentColumn++;

                if (currentColumn == columns) {
                    currentColumn = 0;
                    currentLine++;
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return image;
    }

    ArrayList<BufferedImage> getBufferedImageList() {
        return this.bufferedImageList;
    }
}

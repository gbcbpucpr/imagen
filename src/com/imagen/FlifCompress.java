/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

class FlifCompress {

    FlifCompress(FastaFileReader fastaFileReader) {

        StringBuilder sequence = fastaFileReader.getAtcgSequence();

        AtcgToColorMatrix atcgToColorMatrix = new AtcgToColorMatrix(sequence, false);

        flifGenerator(atcgToColorMatrix.getBufferedImageList(), fastaFileReader);
    }

    private void flifGenerator(ArrayList<BufferedImage> bufferedImageList, FastaFileReader fastaFileReader) {

        boolean multiParts = false;

        if (bufferedImageList.size() > 1) {
            multiParts = true;
        }

        int index = -1;
        String part = "_part_";

        try {

            for (BufferedImage bufferedImage : bufferedImageList) {

                String partName = "";

                if (multiParts) {
                    index += 1;
                    partName = part + index;
                }

                Path flifPath = Paths.get(fastaFileReader.getFolderOutput() + fastaFileReader.getFastaName() + partName + ".flif");

                Path pngPath = pngGenerator(bufferedImage, flifPath);

                File imagePng = new File(String.valueOf(pngPath));
                if (imagePng.exists()) {
                    String imgDestino = String.valueOf(flifPath);
                    Process p = Runtime.getRuntime().exec("flif -e -Q100 " + pngPath.toString() + " " + imgDestino);
                    p.waitFor();
                }

                imagePng.delete();

                if (imagePng.exists()) {
                    System.out.println("Failed to delete the png file on compression.");
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    private Path pngGenerator(BufferedImage bufferedImage, Path flifPath) {

        Path imagePath = null;

        try {

            imagePath = Paths.get(flifPath.toAbsolutePath().toString().replaceFirst("[.][^.]+$", "")+".png");

            File newPngImage = new File(String.valueOf(imagePath));

            FileOutputStream os = new FileOutputStream(newPngImage);

            ImageWriter writer = ImageIO.getImageWritersByFormatName("png").next();

            if (writer == null) {
                throw new RuntimeException("No available png writer!");
            }

            final ImageOutputStream ios = ImageIO.createImageOutputStream(os);

            writer.setOutput(ios);

            writer.write(null, new IIOImage(bufferedImage, null, null), null);

            writer.dispose();
            ios.flush();
            ios.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return imagePath;
    }
}

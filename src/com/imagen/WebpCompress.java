/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import com.luciad.imageio.webp.WebPWriteParam;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

class WebpCompress {

    WebpCompress(FastaFileReader fastaFileReader) {

        StringBuilder sequence = fastaFileReader.getAtcgSequence();

        AtcgToColorMatrix atcgToColorMatrix = new AtcgToColorMatrix(sequence, true);

        webpGenerator(atcgToColorMatrix.getBufferedImageList(), fastaFileReader);
    }

    private void webpGenerator(ArrayList<BufferedImage> bufferedImageList, FastaFileReader fastaFileReader) {

        boolean multiParts = false;

        if (bufferedImageList.size() > 1) {
            multiParts = true;
        }

        int index = -1;
        String part = "_part_";

        try {

            for (BufferedImage bufferedImage : bufferedImageList) {

                // Obtain a WebP ImageWriter instance
                ImageWriter writer = ImageIO.getImageWritersByMIMEType("image/webp").next();

                // configure encoding parameters
                WebPWriteParam writeParam = new WebPWriteParam(writer.getLocale());
                writeParam.setCompressionMode(WebPWriteParam.MODE_EXPLICIT);
                writeParam.setCompressionType("Lossless");
                writeParam.setCompressionQuality(0.0F);

                String partName = "";

                if (multiParts) {
                    index += 1;
                    partName = part + index;
                }

                Path imagePath = Paths.get(fastaFileReader.getFolderOutput() + fastaFileReader.getFastaName() + partName + ".webp");

                // configure the output on the ImageWriter
                File newImage = new File(String.valueOf(imagePath));

                writer.setOutput(new FileImageOutputStream(newImage));

                // Encoder
                writer.write(null, new IIOImage(bufferedImage, null, null), writeParam);

            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}

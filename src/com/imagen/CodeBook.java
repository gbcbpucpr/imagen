/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import javafx.util.Pair;

import java.util.Collections;
import java.util.LinkedHashMap;

public class CodeBook {

    private String fastaHeader;
    private int sequenceLength;
    private int columnsLength;
    private String pathToSave;
    private Config config = Config.getInstance();
    private StringBuilder nonAtcgMap;

    private LinkedHashMap<Integer, Pair> transforms;

    private static CodeBook instance = null;

    private CodeBook() {
        // Avoid create an instance outside of this class
    }


    public static CodeBook getInstance() {
        if (CodeBook.instance == null) {
            CodeBook.instance = new CodeBook();
        }

        return CodeBook.instance;
    }

    public void init(FastaFileReader fastaFileReader) {
        this.transforms = new LinkedHashMap<>();
        this.fastaHeader = fastaFileReader.getFastaHeader();
        this.sequenceLength = fastaFileReader.getAtcgSequence().length();
        this.columnsLength = fastaFileReader.getColumnsLength();
        this.pathToSave = fastaFileReader.getFolderOutput() + fastaFileReader.getFastaName() + config.extensionCodebook;
        this.nonAtcgMap = fastaFileReader.getNonAtcgMap();
    }

    public void init(CodeBookReader codeBookReader) {
        this.fastaHeader = codeBookReader.getFastaHeader();
        this.columnsLength = codeBookReader.getColumnsLength();
        this.sequenceLength = codeBookReader.getSequenceLength();
        this.transforms = codeBookReader.getTransforms();

    }

    String getFastaHeader() {
        return this.fastaHeader;
    }

    int getSequenceLength() {
        return this.sequenceLength;
    }

    int getColumnsLength() {
        return this.columnsLength;
    }

    String getPathToSave() {
        return pathToSave;
    }

    StringBuilder getNonAtcgMap() {
        return nonAtcgMap;
    }

    public LinkedHashMap<Integer, Pair> getTransforms() {
        return transforms;
    }

    public void putTransform(String transformKey, String transformValue) {

        boolean exists = false;

        if (this.transforms.size() > 0) {
            for (int i = 0; i < this.transforms.size(); i++) {

                if (this.transforms.containsKey(i + 1)) {
                    if (this.transforms.get(i + 1).getKey().equals(transformKey)) {
                        this.transforms.put(i + 1, new Pair(transformKey, transformValue));
                        exists = true;
                        break;
                    }
                }

            }
        }

        if (!exists) {

            int max = 0;

            if (this.transforms.size() > 0) {
                max = Collections.max(this.transforms.keySet());
            }

            this.transforms.put(max + 1, new Pair(transformKey, transformValue));
        }
    }

    public void putTransformKeys(String keys) {

        this.transforms.clear();

        for (int i = 0; i < keys.length(); i++) {

            Pair p = new Pair(String.valueOf(keys.charAt(i)), "NULL");

            this.transforms.put(i + 1, p);
        }
    }

    public void setTransforms(LinkedHashMap<Integer, Pair> transforms) {
        this.transforms = transforms;
    }
}

/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import com.imagen.transformers.TransformerFactory;

public class Application {

    public static void main(String[] args) {

//        String file = "NC_000022.11";
//        // String file = "JY157487.1";
//        String format = "w";
        boolean transform = true;
//        boolean decompress = false;
//        String transformers = "bcbr";
//
//        HashMap<String, String> type = new HashMap<>();
//        type.put("w", "webp");
//        type.put("f", "flif");
//
//        String arg0 = "-a";
//        String arg1 = "c";
//        String arg2 = "-i";
//        String arg3 = "/Users/juliano/Projetos/Java/ImaGen/test/original/" + file + ".fasta";
//        String arg4 = "-o";
//        String arg5 = "/Users/juliano/Projetos/Java/ImaGen/test/compression/";
//        String arg6 = "-f";
//        String arg7 = format;
//        String arg8 = "-t";
//        String arg9 = transformers;
//
//        if (decompress) {
//            arg1 = "d";
//            arg3 = "/Users/juliano/Projetos/Java/ImaGen/test/compression/" + file + ".fasta." + type.get(format);
//            arg5 = "/Users/juliano/Projetos/Java/ImaGen/test/decompression/";
//        }
//
//        args = new String[]{arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9};

        CommandValidator commandValidator = new CommandValidator(args);

        if (commandValidator.isCompress()) {
            FastaFileReader fastaFileReader = new FastaFileReader(commandValidator);
            CodeBook codeBook = CodeBook.getInstance();
            codeBook.init(fastaFileReader);


            if (transform) {
                codeBook.putTransformKeys(commandValidator.getTransformTechniques());
                TransformerFactory transformerFactory = new TransformerFactory();
                StringBuilder s = transformerFactory.encode(fastaFileReader.getAtcgSequence(), codeBook.getTransforms());
                fastaFileReader.setAtcgSequence(s);
            }
            CodeBookWriter codeBookWriter = new CodeBookWriter(codeBook);


            //Huffman h = new Huffman();

            // StringBuilder ss = new StringBuilder("JULIANOVIEIRAMARTINS");


            //h.compress(ss);

            // System.exit(0);

            codeBookWriter.write();

            if (commandValidator.isWebp()) {
                new WebpCompress(fastaFileReader);
            } else {
                new FlifCompress(fastaFileReader);
            }
        } else {
            CodeBookReader codebookFileReader = new CodeBookReader(commandValidator);

            CodeBook codeBook = CodeBook.getInstance();
            codeBook.init(codebookFileReader);

            FormatImageReader imageReader;

            if (commandValidator.isWebp()) {
                imageReader = new WebpImageReader(codebookFileReader, commandValidator);
            } else {
                imageReader = new FlifImageReader(codebookFileReader, commandValidator);
            }

            if (transform) {
                TransformerFactory transformerFactory = new TransformerFactory();
                StringBuilder s = transformerFactory.decode(imageReader.getSequence(), codeBook.getTransforms());
                imageReader.setSequence(s);
            }
            new FastaFileWriter(imageReader, codebookFileReader, commandValidator);
        }
    }
}

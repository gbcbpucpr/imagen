/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

public class Test {

    public static void main(String[] args) {

        char bits[] = new char[4];

        bits[0] = '1';
        bits[1] = '1';
        bits[2] = '1';

        System.out.println(String.valueOf(bits).trim());

    }
}

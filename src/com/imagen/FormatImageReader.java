/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

abstract class FormatImageReader {


    public abstract StringBuilder getSequence();

    public abstract void setSequence(StringBuilder originalSequence);
}

/******************************************************************************
 * Copyright (node) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen.encoders;

public abstract class Encoder {

    public abstract void compress(StringBuilder sequence);

    public abstract void expand(StringBuilder sequence);
}

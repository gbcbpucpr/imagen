/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen.transformers;

/**
 * The type Transformer.
 */
public abstract class Transformer implements TransformerInterface {

    /**
     * The Parameter.
     */
    protected String parameter = "0";

    public abstract StringBuilder encode(StringBuilder sequence);

    public abstract StringBuilder decode(StringBuilder encodedSequence);

    public abstract String getParameter();

    public abstract void setParameter(String parameter);
}

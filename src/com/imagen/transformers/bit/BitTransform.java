/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen.transformers.bit;

import com.imagen.transformers.mtf.MoveToFront;

public class BitTransform {

    public static void main(String[] args) {

        StringBuilder sequence = new StringBuilder("ggatcttgagcggcaagcggggcgcaaagggcggggacggagcgcgcaaggatgcggagttcggcgtgac");

        int step = 70;

        String dictionary = "atcg";

        encode(sequence, step, dictionary);
    }

    public static StringBuilder encode(StringBuilder sequence, int step, String dictionary) {

        StringBuilder encodedSequence = new StringBuilder();

        MoveToFront mtf = new MoveToFront(dictionary);

        int i = 0;

        while ((i + step - 1) < sequence.length()) {

            StringBuilder substring = new StringBuilder(sequence.substring(i, i + step));

            StringBuilder mtfEncoded = mtf.encode(substring);

            System.out.println(mtfEncoded);

            i += step;

            StringBuilder bitString = new StringBuilder();

            int j = 1;
            char lastC = mtfEncoded.charAt(0);
            while (j < mtfEncoded.length()) {

                char currentC = mtfEncoded.charAt(j);


                bitString.append(distance(currentC, lastC, dictionary));

                lastC = currentC;

                j += 1;
            }

            System.out.println(bitString);

        }

        return encodedSequence;
    }

    public static StringBuilder distance(char currentC, char lastC, String dictionary) {

        StringBuilder bitString = new StringBuilder();

        int i = dictionary.indexOf(lastC);
        while (i < dictionary.length()) {

            char c = dictionary.charAt(i);


            if (c == currentC) {
                bitString.append("1");
                return bitString;
            }

            bitString.append("0");


            i += 1;

            if (i == dictionary.length()) {
                i = 0;
            }
        }
        return bitString;
    }
}

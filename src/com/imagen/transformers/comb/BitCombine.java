/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen.transformers.comb;

import com.imagen.tools.converters.SymbolBit;
import com.imagen.transformers.Transformer;

/**
 * The type Bit combine.
 */
public class BitCombine extends Transformer {

    /**
     * Instantiates a new Bit combine.
     *
     * @param parameter the parameter
     */
    public BitCombine(String parameter) {
        this.parameter = parameter;
    }

    @Override
    public StringBuilder encode(StringBuilder sequence) {

        // int combineLength = Integer.parseInt(parameter);
        int combineLength = 4;

        if (sequence.length() < (combineLength / 2.0)) {
            try {
                throw new Exception("Sequence length invalid to bit combine size.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        StringBuilder bitString = new StringBuilder();

        for (int i = 0; i < sequence.length(); i++) {
            char symbol = sequence.charAt(i);
            bitString.append(SymbolBit.symbolToBit4(symbol));
        }

        StringBuilder newSequence = new StringBuilder();

        char bits[] = new char[combineLength];
        int index = 0;
        for (int i = 0; i < bitString.length(); i++) {
            char bit = bitString.charAt(i);
            bits[index] = bit;
            index += 1;
            if (index == combineLength) {
                index = 0;
                newSequence.append(SymbolBit.bitsToAscii(bits));
                bits = new char[combineLength];
            }
        }

        parameter += String.valueOf(bits).trim();
        return newSequence;
    }

    @Override
    public StringBuilder decode(StringBuilder encodedSequence) {

        int combineLength = Character.getNumericValue(parameter.charAt(0));


        StringBuilder newSequence = new StringBuilder();
        StringBuilder bitString = new StringBuilder();

        for (int i = 0; i < encodedSequence.length(); i++) {
            char symbol = encodedSequence.charAt(i);
            bitString.append(SymbolBit.asciiToBits(symbol, combineLength));
        }

        if (parameter.length() > 1) {
            bitString.append(parameter.substring(1));
        }

        for (int i = 0; i < bitString.length() - 1; i += 2) {
            char bit1 = bitString.charAt(i);
            char bit2 = bitString.charAt(i + 1);
            String bits = String.valueOf(bit1) + String.valueOf(bit2);
            char c = SymbolBit.bitToSymbol4(bits);
            newSequence.append(c);
        }
        return newSequence;
    }

    @Override
    public String getParameter() {
        return null;
    }

    @Override
    public void setParameter(String parameter) {

    }

//    public static void main(String[] args) {
//        parameter = "4";
//        StringBuilder s = new StringBuilder("AAAAAAAAAAA");
//        StringBuilder e = encode(s);
//        StringBuilder d = decode(e);
//        System.out.println(e);
//        System.out.println(d);
//        System.out.println(s);
//        System.out.println(parameter);
//    }
}

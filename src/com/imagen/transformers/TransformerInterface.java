/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen.transformers;

public interface TransformerInterface {

    String parameter = null;

    StringBuilder encode(StringBuilder sequence);

    StringBuilder decode(StringBuilder sequence);

    void setParameter(String parameter);

    String getParameter();
}

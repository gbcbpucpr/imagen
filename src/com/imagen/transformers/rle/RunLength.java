/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen.transformers.rle;

import com.imagen.transformers.Transformer;

/**
 * The type Run length.
 */
public class RunLength extends Transformer {

    public RunLength() {
    }

    public RunLength(String parameter) {

        this.parameter = parameter;
    }

    @Override
    public StringBuilder encode(StringBuilder sequence) {

        StringBuilder compressed = new StringBuilder();

        for (int i = 0; i < sequence.length(); i++) {
            int runLength = 1;
            while (i + 1 < sequence.length() && sequence.charAt(i) == sequence.charAt(i + 1)) {
                runLength++;
                i++;
            }
            if (runLength > 1) {
                compressed.append(runLength);
            }
            compressed.append(sequence.charAt(i));
        }
        return compressed;
    }

    @Override
    public StringBuilder decode(StringBuilder encodedSequence) {

        StringBuilder decoded = new StringBuilder();

        StringBuilder runLength = new StringBuilder();

        for (int i = 0; i < encodedSequence.length(); i++) {
            if (Character.isDigit(encodedSequence.charAt(i))) {
                runLength.append(encodedSequence.charAt(i));
            } else {
                int freq = 1;
                if (runLength.length() > 0) {
                    freq = Integer.parseInt(String.valueOf(runLength));
                }
                for (int j = 0; j < freq; j++) {
                    decoded.append(encodedSequence.charAt(i));
                }
                runLength.setLength(0);
            }
        }

        return decoded;
    }

    @Override
    public String getParameter() {
        return null;
    }

    @Override
    public void setParameter(String parameter) {

    }

//    public static void main(String[] args) {
//        StringBuilder s = new StringBuilder("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATGCC");
//        StringBuilder e = encode(s);
//        StringBuilder r = decode(e);
//        System.out.println(s.toString().equals(r.toString()));
//        System.out.println(e);
//        System.out.println(r);
//    }
}

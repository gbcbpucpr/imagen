/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen.transformers.bwt;

import com.imagen.transformers.Transformer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class BurrowsWheeler extends Transformer {

    public BurrowsWheeler() {
    }

    public BurrowsWheeler(String parameter) {
        this.parameter = parameter;
    }

    @Override
    public StringBuilder encode(StringBuilder sequence) {

        StringBuilder newSequence = new StringBuilder();

        CircularSuffixArray circularSuffixArray = new CircularSuffixArray(sequence);

        this.parameter = String.valueOf(circularSuffixArray.getFirst());

        // make output as last chars of sorted suffixes
        for (int i = 0; i < circularSuffixArray.length(); i++) {
            int index = circularSuffixArray.index(i);
            if (index == 0) {
                //BinaryStdOut.write(input.charAt(input.length() - 1), CHAR_BITS);
                newSequence.append(Character.toString(sequence.charAt(sequence.length() - 1)));
                continue;
            }
            //BinaryStdOut.write(input.charAt(index - 1), CHAR_BITS);
            newSequence.append(Character.toString(sequence.charAt(index - 1)));
        }

        return newSequence;
    }

    /**
     * Apply Burrows-Wheeler decoding, reading from standard input and writing to standard output
     */
    @Override
    public StringBuilder decode(StringBuilder encodedSequence) {

        StringBuilder originalSequence = new StringBuilder();

        // take first, t[] from input
        char[] t = new char[encodedSequence.length()];

        encodedSequence.getChars(0, encodedSequence.length(), t, 0);

        char letter = encodedSequence.charAt(Integer.parseInt(parameter));

        encodedSequence = null;

        // construct next[]
        int next[] = new int[t.length];

        // Algorithm: Brute Force requires O(n^2) =>
        // go through t, consider t as key remember positions of t's in the Queue
        Map<Character, Queue<Integer>> positions = new HashMap<>();

        for (int i = 0; i < t.length; i++) {

            if (!positions.containsKey(t[i])) {

                positions.put(t[i], new Queue<>());
            }

            positions.get(t[i]).enqueue(i);
        }

        // get first chars array
        Arrays.sort(t);

        // go consistently through sorted firstChars array
        for (int i = 0; i < t.length; i++) {

            next[i] = positions.get(t[i]).dequeue();
        }

        // decode msg
        // for length of the msg
        for (int i = 0, curRow = Integer.parseInt(parameter); i < t.length; i++, curRow = next[curRow])
        // go from first to next.
        {
            originalSequence.append(t[curRow]);
        }

        // originalSequence.setCharAt(first, letter);
        return originalSequence;
    }

    @Override
    public String getParameter() {
        return this.parameter;
    }

    @Override
    public void setParameter(String parameter) {
        this.parameter = parameter;
    }
}

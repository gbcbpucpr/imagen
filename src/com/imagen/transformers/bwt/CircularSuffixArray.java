/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen.transformers.bwt;

import java.util.Arrays;

public class CircularSuffixArray {

    private StringBuilder input;

    private Integer[] index;

    /**
     * Constructs circular suffix array of input String s
     *
     * @param stringBuilder Input String
     */
    public CircularSuffixArray(StringBuilder stringBuilder) {
        if (stringBuilder == null || stringBuilder.length() == 0) {
            throw new IllegalArgumentException("Can't get suffix array for empty string!");
        }
        input = stringBuilder;
        index = new Integer[length()];
        for (int i = 0; i < index.length; i++) {
            index[i] = i;
        }

        // algorithm: not to store strings; just compare them using number of shifts
        Arrays.sort(index, (fromIndex, toIndex) -> {
            // get start indexes of chars to compare
            int firstIndex = fromIndex;
            int secondIndex = toIndex;
            // for all characters
            for (int i = 0; i < input.length(); i++) {
                // if out of the last char then start from beginning
                if (firstIndex > input.length() - 1) {
                    firstIndex = 0;
                }
                if (secondIndex > input.length() - 1) {
                    secondIndex = 0;
                }
                // if first string > second
                if (input.charAt(firstIndex) < input.charAt(secondIndex)) {
                    return -1;
                } else if (input.charAt(firstIndex) > input.charAt(secondIndex)) {
                    return 1;
                }
                // watch next chars
                firstIndex++;
                secondIndex++;
            }
            // equal strings
            return 0;
        });
    }

    /**
     * Length of circular array string
     */
    public int length() {
        return input.length();
    }

    /**
     * Returns ordinal row (index) in the original suffix of ith sorted suffix
     *
     * @param i Ordinal number of sorted index
     */
    public int index(int i) {
        return index[i];
    }

    public int getFirst() {
        return Arrays.asList(index).indexOf(0);
    }
}

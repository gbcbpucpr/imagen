/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen.transformers.mtf;

import com.imagen.transformers.TransformerInterface;

import java.util.HashMap;
import java.util.Map;

public class MoveToFront implements TransformerInterface {

    private Map<Integer, String> intToCharMap;
    private Map<String, Integer> charToIntMap;
    private String parameter = "ATCG";


    public MoveToFront() {

        this.prepareMaps();
    }

    public MoveToFront(String parameter) {

        if (!parameter.equals("NULL") && !parameter.equals(this.parameter)) {
            this.parameter = parameter;
        }

        this.prepareMaps();
    }

    private void prepareMaps() {
        this.intToCharMap = new HashMap<>();

        this.intToCharMap.put(0, "A");
        this.intToCharMap.put(1, "T");
        this.intToCharMap.put(2, "C");
        this.intToCharMap.put(3, "G");

        this.charToIntMap = new HashMap<>();

        this.charToIntMap.put("A", 0);
        this.charToIntMap.put("T", 1);
        this.charToIntMap.put("C", 2);
        this.charToIntMap.put("G", 3);
    }

    @Override
    public StringBuilder encode(StringBuilder atcgSequence) {

        StringBuilder dictionaryTable = new StringBuilder(parameter);

        StringBuilder mtfSequence = new StringBuilder();

        for (int i = 0; i < atcgSequence.length(); i++) {

            char atcgSymbol = atcgSequence.charAt(i);

            int idx = dictionaryTable.indexOf(String.valueOf(atcgSymbol));

            mtfSequence.append(this.intToCharMap.get(idx));

            dictionaryTable.deleteCharAt(idx).insert(0, atcgSymbol);
        }

        return mtfSequence;

    }

    @Override
    public StringBuilder decode(StringBuilder mtfSequence) {

        StringBuilder dictionaryTable = new StringBuilder(parameter);

        StringBuilder atcgSequence = new StringBuilder();

        for (int i = 0; i < mtfSequence.length(); i++) {

            char mtfSymbol = mtfSequence.charAt(i);

            int idx = this.charToIntMap.get(String.valueOf(mtfSymbol));

            char atcgSymbol = dictionaryTable.charAt(idx);

            atcgSequence.append(Character.toString(atcgSymbol));

            dictionaryTable.deleteCharAt(idx).insert(0, atcgSymbol);
        }

        return atcgSequence;
    }

    @Override
    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Override
    public String getParameter() {
        return this.parameter;
    }

//    public void test() {
//
//        String atcgTable = "ATCG";
//
//        StringBuilder toEncode = new StringBuilder("ATCGATCGATCGATCGATCGATCGATCGATCGATCGATCGATCGATCG");
//
//        System.out.println("original: " + toEncode);
//
//
//        StringBuilder encoded = encode(toEncode, atcgTable);
//
//
//        System.out.println(" encoded: " + encoded);
//
//
//        StringBuilder decoded = decode(encoded, atcgTable);
//
//
//        System.out.println(" decoded: " + decoded);
//
//
//        System.out.println(decoded.toString().equals(toEncode.toString()));
//
//    }
}

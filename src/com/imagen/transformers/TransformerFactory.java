/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen.transformers;

import com.imagen.CodeBook;
import com.imagen.Config;
import com.imagen.transformers.bwt.BurrowsWheeler;
import com.imagen.transformers.comb.BitCombine;
import com.imagen.transformers.mtf.MoveToFront;
import com.imagen.transformers.rle.RunLength;
import javafx.util.Pair;

import java.util.LinkedHashMap;

public class TransformerFactory {

    private CodeBook codeBook = CodeBook.getInstance();

    private Config config = Config.getInstance();

    private StringBuilder newSequence;

    public StringBuilder encode(StringBuilder sequence, LinkedHashMap<Integer, Pair> strategies) {

        this.newSequence = sequence;

        for (int i = 0; i < strategies.size(); i++) {

            int k = i + 1;

            String t = String.valueOf(strategies.get(k).getKey());
            String p = String.valueOf(strategies.get(k).getValue());
            TransformerInterface transformerInterface = this.getTransformer(t, p);
            this.newSequence = transformerInterface.encode(this.newSequence);

            this.codeBook.putTransform(t, transformerInterface.getParameter());
        }

        return newSequence;
    }

    public StringBuilder decode(StringBuilder sequence, LinkedHashMap<Integer, Pair> strategies) {

        this.newSequence = sequence;

        for (int i = strategies.size(); i >= 1; i--) {

            int k = i;
            String t = String.valueOf(strategies.get(k).getKey());
            String p = String.valueOf(strategies.get(k).getValue());

            TransformerInterface transformerInterface = this.getTransformer(t, p);
            this.newSequence = transformerInterface.decode(this.newSequence);

            this.codeBook.putTransform(t, transformerInterface.getParameter());
        }

        return this.newSequence;

    }

    private TransformerInterface getTransformer(String transformer, String parameter) {

        if (transformer.equalsIgnoreCase(config.bwtKey)) {
            return new BurrowsWheeler(parameter);
        } else if (transformer.equalsIgnoreCase(config.mtfKey)) {
            return new MoveToFront(parameter);
        } else if (transformer.equalsIgnoreCase(config.rleKey)) {
            return new RunLength(parameter);
        } else if (transformer.equalsIgnoreCase(config.btcKey)) {
            return new BitCombine(parameter);
        }
        return null;
    }
}

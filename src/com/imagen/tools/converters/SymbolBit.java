/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen.tools.converters;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Symbol bit.
 */
public class SymbolBit {

    private static final Map<Character, String> bitMap64;

    static {
        Map<Character, String> aMap64 = new HashMap<>();
        aMap64.put('0', "000000");
        aMap64.put('1', "000001");
        aMap64.put('2', "000010");
        aMap64.put('3', "000011");
        aMap64.put('4', "000100");
        aMap64.put('5', "000101");
        aMap64.put('6', "000110");
        aMap64.put('7', "000111");
        aMap64.put('8', "001000");
        aMap64.put('9', "001001");
        aMap64.put('a', "001010");
        aMap64.put('b', "001011");
        aMap64.put('c', "001100");
        aMap64.put('d', "001101");
        aMap64.put('e', "001110");
        aMap64.put('f', "001111");
        aMap64.put('g', "010000");
        aMap64.put('h', "010001");
        aMap64.put('i', "010010");
        aMap64.put('j', "010011");
        aMap64.put('k', "010100");
        aMap64.put('l', "010101");
        aMap64.put('m', "010110");
        aMap64.put('n', "010111");
        aMap64.put('o', "011000");
        aMap64.put('p', "011001");
        aMap64.put('q', "011010");
        aMap64.put('r', "011011");
        aMap64.put('s', "011100");
        aMap64.put('t', "011101");
        aMap64.put('u', "011110");
        aMap64.put('v', "011111");
        aMap64.put('w', "100000");
        aMap64.put('x', "100001");
        aMap64.put('y', "100010");
        aMap64.put('z', "100011");
        aMap64.put('A', "100100");
        aMap64.put('B', "100101");
        aMap64.put('C', "100110");
        aMap64.put('D', "100111");
        aMap64.put('E', "101000");
        aMap64.put('F', "101001");
        aMap64.put('G', "101010");
        aMap64.put('H', "101011");
        aMap64.put('I', "101100");
        aMap64.put('J', "101101");
        aMap64.put('K', "101110");
        aMap64.put('L', "101111");
        aMap64.put('M', "110000");
        aMap64.put('N', "110001");
        aMap64.put('O', "110010");
        aMap64.put('P', "110011");
        aMap64.put('Q', "110100");
        aMap64.put('R', "110101");
        aMap64.put('S', "110110");
        aMap64.put('T', "110111");
        aMap64.put('U', "111000");
        aMap64.put('V', "111001");
        aMap64.put('W', "111010");
        aMap64.put('X', "111011");
        aMap64.put('Y', "111100");
        aMap64.put('Z', "111101");
        aMap64.put('!', "111110");
        aMap64.put('@', "111111");
        bitMap64 = Collections.unmodifiableMap(aMap64);
    }

    private static final Map<Character, String> bitMap32;

    static {
        Map<Character, String> aMap32 = new HashMap<>();
        aMap32.put('0', "00000");
        aMap32.put('1', "00001");
        aMap32.put('2', "00010");
        aMap32.put('3', "00011");
        aMap32.put('4', "00100");
        aMap32.put('5', "00101");
        aMap32.put('6', "00110");
        aMap32.put('7', "00111");
        aMap32.put('8', "01000");
        aMap32.put('9', "01001");
        aMap32.put('a', "01010");
        aMap32.put('b', "01011");
        aMap32.put('c', "01100");
        aMap32.put('d', "01101");
        aMap32.put('e', "01110");
        aMap32.put('f', "01111");
        aMap32.put('g', "10000");
        aMap32.put('h', "10001");
        aMap32.put('i', "10010");
        aMap32.put('j', "10011");
        aMap32.put('k', "10100");
        aMap32.put('l', "10101");
        aMap32.put('m', "10110");
        aMap32.put('n', "10111");
        aMap32.put('o', "11000");
        aMap32.put('p', "11001");
        aMap32.put('q', "11010");
        aMap32.put('r', "11011");
        aMap32.put('s', "11100");
        aMap32.put('t', "11101");
        aMap32.put('u', "11110");
        aMap32.put('v', "11111");
        bitMap32 = Collections.unmodifiableMap(aMap32);
    }

    private static final Map<Character, String> bitMap16;

    static {
        Map<Character, String> aMap16 = new HashMap<>();
//        aMap16.put('0', "0000");
//        aMap16.put('1', "0001");
//        aMap16.put('2', "0010");
//        aMap16.put('3', "0011");
//        aMap16.put('4', "0100");
//        aMap16.put('5', "0101");
//        aMap16.put('6', "0110");
//        aMap16.put('7', "0111");
//        aMap16.put('8', "1000");
//        aMap16.put('9', "1001");
//        aMap16.put('a', "1010");
//        aMap16.put('b', "1011");
//        aMap16.put('c', "1100");
//        aMap16.put('d', "1101");
//        aMap16.put('e', "1110");
//        aMap16.put('f', "1111");
        aMap16.put('g', "0000");
        aMap16.put('h', "0001");
        aMap16.put('i', "0010");
        aMap16.put('j', "0011");
        aMap16.put('k', "0100");
        aMap16.put('l', "0101");
        aMap16.put('m', "0110");
        aMap16.put('n', "0111");
        aMap16.put('o', "1000");
        aMap16.put('p', "1001");
        aMap16.put('a', "1010");
        aMap16.put('b', "1011");
        aMap16.put('c', "1100");
        aMap16.put('d', "1101");
        aMap16.put('e', "1110");
        aMap16.put('f', "1111");
        bitMap16 = Collections.unmodifiableMap(aMap16);
    }

    private static final Map<Character, String> bitMap8;

    static {
        Map<Character, String> aMap8 = new HashMap<>();
        aMap8.put('0', "000");
        aMap8.put('1', "001");
        aMap8.put('2', "010");
        aMap8.put('3', "011");
        aMap8.put('4', "100");
        aMap8.put('5', "101");
        aMap8.put('6', "110");
        aMap8.put('7', "111");
        bitMap8 = Collections.unmodifiableMap(aMap8);
    }

    private static final Map<Character, String> bitMap4;

    static {
        Map<Character, String> aMap4 = new HashMap<>();
        aMap4.put('A', "00");
        aMap4.put('T', "01");
        aMap4.put('C', "10");
        aMap4.put('G', "11");
        bitMap4 = Collections.unmodifiableMap(aMap4);
    }


    /**
     * Symbol to bit 64 string.
     *
     * @param symbol the symbol
     * @return the string
     */
    public static String symbolToBit64(Character symbol) {
        try {
            return bitMap64.get(symbol);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Bit to symbol 64 character.
     *
     * @param bit the bit
     * @return the character
     */
    public static Character bitToSymbol64(String bit) {
        return getBit(bit, bitMap64);
    }

    /**
     * Symbol to bit 32 string.
     *
     * @param symbol the symbol
     * @return the string
     */
    public static String symbolToBit32(Character symbol) {
        try {
            return bitMap32.get(symbol);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Bit to symbol 32 character.
     *
     * @param bit the bit
     * @return the character
     */
    public static Character bitToSymbol32(String bit) {
        return getBit(bit, bitMap32);
    }

    /**
     * Symbol to bit 16 string.
     *
     * @param symbol the symbol
     * @return the string
     */
    public static String symbolToBit16(Character symbol) {
        try {
            return bitMap16.get(symbol);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Bit to symbol 16 character.
     *
     * @param bit the bit
     * @return the character
     */
    public static Character bitToSymbol16(String bit) {
        return getBit(bit, bitMap16);
    }

    /**
     * Symbol to bit 8 string.
     *
     * @param symbol the symbol
     * @return the string
     */
    public static String symbolToBit8(Character symbol) {
        try {
            return bitMap8.get(symbol);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Bit to symbol 8 character.
     *
     * @param bit the bit
     * @return the character
     */
    public static Character bitToSymbol8(String bit) {
        return getBit(bit, bitMap8);
    }

    /**
     * Symbol to bit 4 string.
     *
     * @param symbol the symbol
     * @return the string
     */
    public static String symbolToBit4(Character symbol) {
        try {
            return bitMap4.get(symbol);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Bit to symbol 4 character.
     *
     * @param bit the bit
     * @return the character
     */
    public static Character bitToSymbol4(String bit) {
        return getBit(bit, bitMap4);
    }

    /**
     * Get bit character
     *
     * @param bit    the bit
     * @param bitMap the bitMap
     * @return the character
     */
    private static Character getBit(String bit, Map<Character, String> bitMap) {
        try {
            for (Map.Entry<Character, String> entry : bitMap.entrySet()) {
                if (bit.equals(entry.getValue())) {
                    return entry.getKey();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Bits to ascii character.
     *
     * @param bits the bits
     * @return the character
     */
    public static Character bitsToAscii(char[] bits) {
        Character asciiSymbol = null;
        if (bits.length == 2) {
            asciiSymbol = bitToSymbol4(String.valueOf(bits));
        } else if (bits.length == 3) {
            asciiSymbol = bitToSymbol8(String.valueOf(bits));
        } else if (bits.length == 4) {
            asciiSymbol = bitToSymbol16(String.valueOf(bits));
        } else if (bits.length == 5) {
            asciiSymbol = bitToSymbol32(String.valueOf(bits));
        } else if (bits.length == 6) {
            asciiSymbol = bitToSymbol64(String.valueOf(bits));
        }
        return asciiSymbol;
    }

    /**
     * Ascii to bits string.
     *
     * @param character the character
     * @param bitLength the bit length
     * @return the string
     */
    public static String asciiToBits(Character character, int bitLength) {
        String bitString = null;
        if (bitLength == 2) {
            bitString = symbolToBit4(character);
        } else if (bitLength == 3) {
            bitString = symbolToBit8(character);
        } else if (bitLength == 4) {
            bitString = symbolToBit16(character);
        } else if (bitLength == 5) {
            bitString = symbolToBit32(character);
        } else if (bitLength == 6) {
            bitString = symbolToBit64(character);
        }
        return bitString;
    }
}
/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen.tools.converters;

import java.awt.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * The type SymbolColor.
 */
public class SymbolColor {

    private static final Map<Character, Color> colorMap;

    static {
        Map<Character, Color> aMap = new HashMap<>();
        aMap.put('0', new Color(0, 0, 0));
        aMap.put('1', new Color(1, 0, 0));
        aMap.put('2', new Color(2, 0, 0));
        aMap.put('3', new Color(3, 0, 0));
        aMap.put('4', new Color(4, 0, 0));
        aMap.put('5', new Color(5, 0, 0));
        aMap.put('6', new Color(6, 0, 0));
        aMap.put('7', new Color(7, 0, 0));
        aMap.put('8', new Color(8, 0, 0));
        aMap.put('9', new Color(9, 0, 0));
        aMap.put('a', new Color(10, 0, 0));
        aMap.put('b', new Color(11, 0, 0));
        aMap.put('c', new Color(12, 0, 0));
        aMap.put('d', new Color(13, 0, 0));
        aMap.put('e', new Color(14, 0, 0));
        aMap.put('f', new Color(15, 0, 0));
        aMap.put('g', new Color(16, 0, 0));
        aMap.put('h', new Color(17, 0, 0));
        aMap.put('i', new Color(18, 0, 0));
        aMap.put('j', new Color(19, 0, 0));
        aMap.put('k', new Color(20, 0, 0));
        aMap.put('l', new Color(21, 0, 0));
        aMap.put('m', new Color(22, 0, 0));
        aMap.put('n', new Color(23, 0, 0));
        aMap.put('o', new Color(24, 0, 0));
        aMap.put('p', new Color(25, 0, 0));
        aMap.put('q', new Color(26, 0, 0));
        aMap.put('r', new Color(27, 0, 0));
        aMap.put('s', new Color(28, 0, 0));
        aMap.put('t', new Color(29, 0, 0));
        aMap.put('u', new Color(30, 0, 0));
        aMap.put('v', new Color(31, 0, 0));
        aMap.put('w', new Color(32, 0, 0));
        aMap.put('x', new Color(33, 0, 0));
        aMap.put('y', new Color(34, 0, 0));
        aMap.put('z', new Color(35, 0, 0));
        aMap.put('A', new Color(36, 0, 0));
        aMap.put('B', new Color(37, 0, 0));
        aMap.put('C', new Color(38, 0, 0));
        aMap.put('D', new Color(39, 0, 0));
        aMap.put('E', new Color(40, 0, 0));
        aMap.put('F', new Color(41, 0, 0));
        aMap.put('G', new Color(42, 0, 0));
        aMap.put('H', new Color(43, 0, 0));
        aMap.put('I', new Color(44, 0, 0));
        aMap.put('J', new Color(45, 0, 0));
        aMap.put('K', new Color(46, 0, 0));
        aMap.put('L', new Color(47, 0, 0));
        aMap.put('M', new Color(48, 0, 0));
        aMap.put('N', new Color(49, 0, 0));
        aMap.put('O', new Color(50, 0, 0));
        aMap.put('P', new Color(51, 0, 0));
        aMap.put('Q', new Color(52, 0, 0));
        aMap.put('R', new Color(53, 0, 0));
        aMap.put('S', new Color(54, 0, 0));
        aMap.put('T', new Color(55, 0, 0));
        aMap.put('U', new Color(56, 0, 0));
        aMap.put('V', new Color(57, 0, 0));
        aMap.put('W', new Color(58, 0, 0));
        aMap.put('X', new Color(59, 0, 0));
        aMap.put('Y', new Color(60, 0, 0));
        aMap.put('Z', new Color(61, 0, 0));
        aMap.put('!', new Color(62, 0, 0));
        aMap.put('@', new Color(63, 0, 0));
        colorMap = Collections.unmodifiableMap(aMap);
    }

    /**
     * Color to symbol character.
     *
     * @param color the color
     * @return the character
     */
    public static Character colorToSymbol(Color color) {
        try {
            for (Map.Entry<Character, Color> entry : colorMap.entrySet()) {
                if (color.equals(entry.getValue())) {
                    return entry.getKey();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Symbol to color color.
     *
     * @param symbol the symbol
     * @return the color
     */
    public static Color symbolToColor(Character symbol) {
        try {
            return colorMap.get(symbol);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

class FastaFileWriter {

    FastaFileWriter(FormatImageReader imageReader, CodeBookReader codebookFileReader, CommandValidator commandValidator) {

        try {

            StringBuilder sequence = imageReader.getSequence();

            if (!codebookFileReader.getCodeBookLines().isEmpty()) {
                int lastPosition = 0;
                for (CodeBookLine codeBook : codebookFileReader.getCodeBookLines()) {
                    lastPosition += codeBook.getDelta();
                    String letter = String.valueOf(codeBook.getNucleotide()).toUpperCase();
                    String repetitions = new String(new char[codeBook.getLength()]).replace("\0", letter);
                    sequence.insert(lastPosition, repetitions);
                }
            }

            int columnLength = codebookFileReader.getColumnsLength();

            Path path = Paths.get(commandValidator.getInputPath());

            String fastaName = path.getFileName().toString();

            String fastaPath = commandValidator.getOutputPath() + fastaName.replaceFirst("[.][^.]+$", "");

            File fastaFile = new File(fastaPath);

            FileOutputStream fileOutputStream = new FileOutputStream(fastaFile);

            String regExp = "(.{" + columnLength + "})";

            String sequenceWithBreakLines = sequence.toString().replaceAll(regExp, "$1\n");

            OutputStreamWriter osw = new OutputStreamWriter(fileOutputStream);

            String fastaHeader = codebookFileReader.getFastaHeader();

            osw.write(fastaHeader);
            osw.write("\n");
            osw.write(sequenceWithBreakLines);
            osw.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }
}

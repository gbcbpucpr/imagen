/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import java.io.File;

public class CodeBookZip {

    public static void encode(File file, String completePath) {

        try {
            if (file.isFile()) {

                String command = "7za a -t7z " + completePath + " " + file.toString() + " -m0=PPMd -sdel";
                final Runtime r = Runtime.getRuntime();
                final Process p = r.exec(command);
                p.waitFor();
                File compressedCodebook = new File(completePath);
                if (!compressedCodebook.exists() || !compressedCodebook.isFile()) {
                    throw new Exception("Invalid: " + compressedCodebook.toString());

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void decode(File input, String outputDirectory) {

        String outputPath = outputDirectory + input.getName().replaceFirst("[.][^.]+$", "");

        try {
            if (input.isFile()) {
                String command = "7z e -y " + input.getAbsolutePath() + " -o" + outputDirectory;
                final Runtime r = Runtime.getRuntime();
                final Process p = r.exec(command);
                p.waitFor();
                File codeBook = new File(outputPath);
                if (!codeBook.exists() || !codeBook.isFile()) {
                    throw new Exception("Invalid: " + codeBook.toString());

                }
            } else {
                System.out.println("Error" + input.getAbsolutePath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import org.apache.commons.cli.*;

class CommandValidator {

    private boolean isCompress = true;
    private boolean isWebp = true;
    private String inputPath;
    private String outputPath;
    private Options options;
    private String transformTechniques = null;

    Config config = Config.getInstance();

    CommandValidator(String[] args) {

        this.validate(args);
    }

    private void validate(String[] args) {

        this.options = new Options();

        Option action = Option.builder("a")
                .required(true)
                .longOpt("action")
                .desc("Use 'c' to isCompress or 'd' to decompress.")
                .hasArg(true)
                .argName("chosen action")
                .build();
        this.options.addOption(action);

        Option inputFile = Option.builder("i")
                .required(true)
                .longOpt("input")
                .desc("FASTA input file.")
                .hasArg(true)
                .argName("input file")
                .build();
        this.options.addOption(inputFile);

        Option outputFolder = Option.builder("o")
                .required(true)
                .longOpt("output")
                .desc("Output folder to save image.")
                .hasArg(true)
                .argName("output folder")
                .build();
        this.options.addOption(outputFolder);

        Option imageFormat = Option.builder("f")
                .required(true)
                .longOpt("format")
                .desc("Image formats. 'f' to flif or 'w' to webp.")
                .hasArg(true)
                .argName("image format")
                .build();
        this.options.addOption(imageFormat);

        Option transform = Option.builder("t")
                .required(true)
                .longOpt("transform")
                .desc("Transform techniques. E.g. 'b' to BWT or 'mb' to MTF+BWT, etc.")
                .hasArg(true)
                .argName("Transform techniques")
                .build();
        this.options.addOption(transform);

        DefaultParser parser = new DefaultParser();

        try {

            CommandLine line = parser.parse(this.options, args);

            // has the arguments been passed?
            if (line.hasOption('a') && line.hasOption('i') && line.hasOption('o') && line.hasOption('f')) {
                // Validate action
                String a = line.getOptionValue('a');

                if (!a.equals("c") && !a.equals("d")) {
                    throw new ParseException("Use 'c' to compress or 'd' to decompress.");
                }

                if (!a.equals("c")) {
                    this.isCompress = false;
                }

                String f = line.getOptionValue('f');

                if (!f.equals("f") && !f.equals("w")) {
                    throw new ParseException("Image formats. f = flif and w = isWebp.");
                }

                if (!f.equals("w")) {
                    this.isWebp = false;
                }

                if (this.isCompress) {

                    if (line.hasOption('t')) {

                        String t = line.getOptionValue('t');

                        for (int i = 0; i < t.length(); i++) {

                            if (!config.transforms.contains(String.valueOf(t.charAt(i)))) {
                                throw new ParseException("Transform techniques. E.g. 'b' to BWT or 'mb' to MTF+BWT, etc.");
                            }
                        }
                        this.transformTechniques = t;
                    }
                }


                // Validate input file and output folder
                InOutPathValidator inOutPathValidator = new InOutPathValidator();

                this.inputPath = line.getOptionValue('i');

                inOutPathValidator.validateInputPath(this.inputPath, this.isCompress);

                this.outputPath = line.getOptionValue('o');

                inOutPathValidator.validateOutputFolder(this.outputPath);
            } else {
                throw new ParseException("Missing parameters");
            }

        } catch (ParseException e) {
            System.err.println("Parsing failed.  Reason: " + e.getMessage());
            printUsageHelp(this.options);
            System.exit(1);
        }
    }

    private void printUsageHelp(Options options) {

        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("The commands in this program are used as follows.", options);

    }

    boolean isCompress() {
        return isCompress;
    }

    boolean isWebp() {
        return isWebp;
    }

    String getInputPath() {
        return inputPath;
    }

    String getOutputPath() {
        return outputPath;
    }

    public String getTransformTechniques() {
        return this.transformTechniques;
    }
}

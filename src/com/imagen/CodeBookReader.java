/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import com.imagen.JSON.JSONObject;
import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class CodeBookReader {

    private List<CodeBookLine> codeBookLines = new ArrayList<>();
    private String fastaHeader;
    private int columnsLength;
    private int sequenceLength;
    private LinkedHashMap<Integer, Pair> transforms;


    CodeBookReader(CommandValidator commandValidator) {

        this.transforms = new LinkedHashMap<>();

        Config config = Config.getInstance();

        File imageFile = new File(commandValidator.getInputPath());

        String codebookPath = imageFile.getParent() + "/" + imageFile.getName().replaceFirst("[.][^.]+$", "");

        codebookPath += config.extensionCodebook;

        String zippedCodebookPath = codebookPath + config.extensionZippedCodebook;

        File zippedCodebook = new File(zippedCodebookPath);

        CodeBookZip.decode(zippedCodebook, commandValidator.getOutputPath());

        String codebookOutputPath = imageFile.getName().replaceFirst("[.][^.]+$", "");

        codebookOutputPath += config.extensionCodebook;

        codebookOutputPath = commandValidator.getOutputPath() + codebookOutputPath;

        try {
            BufferedReader br = new BufferedReader(new FileReader(codebookOutputPath));

            String firstLine = br.readLine();
            JSONObject json = new JSONObject(firstLine);
            this.sequenceLength = json.getInt(config.fastaSequenceLengthKey);
            this.fastaHeader = json.getString(config.fastaHeaderKey);
            this.columnsLength = json.getInt(config.fastaColumnsLengthKey);

            JSONObject jsonTransforms = json.getJSONObject(config.transformersKey);

            Iterator<String> keysItr = jsonTransforms.keys();

            while (keysItr.hasNext()) {

                String key = keysItr.next();

                JSONObject transfom = jsonTransforms.getJSONObject(key);

                Pair p = new Pair(transfom.get("key"), transfom.get("value"));

                this.transforms.put(Integer.parseInt(key), p);
            }

            Pattern pattern = Pattern.compile("(\\d+)([a-zA-Z])(\\d+)");

            String line;
            while ((line = br.readLine()) != null) {
                Matcher matcher = pattern.matcher(line);
                matcher.find();
                CodeBookLine codeBookLine = new CodeBookLine(
                        Integer.valueOf(matcher.group(1)),
                        matcher.group(2).charAt(0),
                        Integer.valueOf(matcher.group(3))
                );
                this.codeBookLines.add(codeBookLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        File codebookFile = new File(codebookOutputPath);
        codebookFile.delete();

        if (codebookFile.exists()) {
            System.out.println("Failed to delete the txt Codebook file on decompression.");
        }
    }

    List<CodeBookLine> getCodeBookLines() {
        return codeBookLines;
    }

    String getFastaHeader() {
        return fastaHeader;
    }

    int getColumnsLength() {
        return columnsLength;
    }

    int getSequenceLength() {
        return sequenceLength;
    }

    public LinkedHashMap<Integer, Pair> getTransforms() {
        return transforms;
    }
}

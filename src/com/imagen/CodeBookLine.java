/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

public class CodeBookLine {

    private int delta;
    private char nucleotide;
    private int length;

    CodeBookLine(int delta, char nucleotide, int length) {
        this.delta = delta;
        this.nucleotide = nucleotide;
        this.length = length;
    }

    public int getDelta() {
        return delta;
    }

    public void setDelta(int delta) {
        this.delta = delta;
    }

    public char getNucleotide() {
        return nucleotide;
    }

    public void setNucleotide(char nucleotide) {
        this.nucleotide = nucleotide;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}

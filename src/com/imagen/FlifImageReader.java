/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

class FlifImageReader extends FormatImageReader {


    private StringBuilder sequence;

    FlifImageReader(CodeBookReader codebookFileReader, CommandValidator commandValidator) {

        this.sequence = new StringBuilder();

        try {

            String pngPath = commandValidator.getInputPath() + ".png";

            Process p = Runtime.getRuntime().exec("flif -d " + commandValidator.getInputPath() + " " + pngPath);

            p.waitFor();

            File pngImageFile = new File(pngPath);

            if (!pngImageFile.exists()) {
                throw new Exception("Failed to convert flif to png.");
            }

            BufferedImage bufferedImage = ImageIO.read(pngImageFile);

            pngImageFile.delete();

            if (pngImageFile.exists()) {
                System.out.println("Failed to delete the png image file on decompression.");
            }

            ImageToAtcgSequence imageToAtcgSequence = new ImageToAtcgSequence(codebookFileReader, bufferedImage);

            this.sequence = imageToAtcgSequence.getSequence();

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

    }

    @Override
    public StringBuilder getSequence() {
        return this.sequence;
    }

    public void setSequence(StringBuilder sequence) {
        this.sequence = sequence;
    }
}

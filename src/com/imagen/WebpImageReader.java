/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import com.luciad.imageio.webp.WebPReadParam;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import java.awt.image.BufferedImage;
import java.io.File;

class WebpImageReader extends FormatImageReader {

    private StringBuilder sequence;

    WebpImageReader(CodeBookReader codebookFileReader, CommandValidator commandValidator) {

        this.sequence = new StringBuilder();

        try {

            // Obtain a WebP ImageReader instance
            ImageReader reader = ImageIO.getImageReadersByMIMEType("image/webp").next();

            // configure decoding parameters
            WebPReadParam readParam = new WebPReadParam();
            readParam.setBypassFiltering(true);

            // configure the input on the ImageReader
            reader.setInput(new FileImageInputStream(new File(commandValidator.getInputPath())));

            // Decode the image
            BufferedImage webpImage = reader.read(0, readParam);

            BufferedImage convertedImage = new BufferedImage(webpImage.getWidth(), webpImage.getHeight(), BufferedImage.TYPE_3BYTE_BGR);

            convertedImage.getGraphics().drawImage(webpImage, 0, 0, null);

            ImageToAtcgSequence imageToAtcgSequence = new ImageToAtcgSequence(codebookFileReader, convertedImage);

            this.sequence = imageToAtcgSequence.getSequence();

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    @Override
    public StringBuilder getSequence() {
        return this.sequence;
    }

    public void setSequence(StringBuilder sequence) {
        this.sequence = sequence;
    }
}

/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Juliano Vieira Martins, Osmar Betazzi Dordal and Kelvin Vieira Kredens.    *
 * - All rights reserved.                                                     *
 ******************************************************************************/

package com.imagen;

import com.imagen.tools.converters.SymbolColor;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

public class ImageToAtcgSequence {

    private StringBuilder sequence;

    ImageToAtcgSequence(CodeBookReader codebookFileReader, BufferedImage bufferedImage) {

        this.sequence = new StringBuilder();

        try {
            byte[] pixels = ((DataBufferByte) bufferedImage.getRaster().getDataBuffer()).getData();

            final int pixelLength = 3;

            for (int pixel = 0; pixel < pixels.length; pixel += pixelLength) {
                int argb = 0;
                argb += -16777216; // 255 alpha
                argb += (int) pixels[pixel] & 0xff; // blue
                argb += ((int) pixels[pixel + 1] & 0xff) << 8; // green
                argb += ((int) pixels[pixel + 2] & 0xff) << 16; // red

                int r = (argb >> 16) & 0xFF;
                int g = (argb >> 8) & 0xFF;
                int b = (argb) & 0xFF;

                this.sequence.append(SymbolColor.colorToSymbol(new Color(r, g, b)));

                if (this.sequence.length() == codebookFileReader.getSequenceLength()) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    public StringBuilder getSequence() {
        return this.sequence;
    }
}

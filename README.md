## ImaGen: Genome Compression Based On Image File Format

### Requirements:

* **Java 8** command: Install from [here](http://www.oracle.com/technetwork/pt/java/javase/downloads/index.html).
* **Flif** command: Install from [here](https://github.com/FLIF-hub/FLIF).
* **7z** command: Install from [here](https://www.7-zip.org/).

### Compression/Decompression Options

```
 -a,--action <chosen action>   Use 'c' to isCompress or 'd' to decompress.
 -f,--format <image format>    Image formats. 'f' to flif and 'w' to webp.
 -i,--input <input file>       FASTA file path (for compression) or IMAGE file path (for decompression) input file.
 -o,--output <output folder>   Output folder path to save IMAGE file or FASTA file.
 -t,--transform <Transform>    Transform techniques. (b, m, r, c))

Examples:

'b' to Burrows-WeellerTransform
'm' to MoveToFront (must run before the RLE, for a while...)
'r' to Run-LengthEncode
'c' to BitCombine

They can be applied joined as well.

'mbc' to MTF+BWT+BTC
'cbm' to BTC+BWT+MTF
```

Example of compression:
```
java -jar [webplib] imagen.jar -a c -f w -i /path/to/file.fasta -o /path/to/output/
```

### Example of decompression:
```
java -jar [webplib] imagen.jar -a d -f w -i /path/to/image.webp -o /path/to/output/
```

**P.S.** For decompression, the codebook (generated in compression) must be in the same folder as the image.

### Path to webp library

This library is in the /lib/ folder of this software.

#### Linux:
```
java -jar -Djava.library.path="/lib/webp/so/" imagen.jar [options]
```
#### Mac Os:
```
java -jar -Djava.library.path="/lib/webp/dylib/" imagen.jar [options]
```
#### Windows:
```
java -jar -Djava.library.path="/lib/webp/dll/" imagen.jar [options]
```